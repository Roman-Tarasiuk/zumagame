﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

using Zuma.GameEngine;

using PointFDraw = System.Drawing.PointF;
using PointFZuma = Zuma.GameEngine.PointF;

namespace Zuma.DesktopUI
{
    public partial class MainForm : Form
    {
        List<PointFZuma> _points;

        public MainForm()
        {
            InitializeComponent();

            InitializeComponentAdv();
        }

        private void InitializeComponentAdv()
        {
            //this.panelMainMenu.Location = new System.Drawing.Point(0, 0);
            //this.panelMainMenu.Size = new System.Drawing.Size(500, 300);
            this.panelMainMenu.Dock = System.Windows.Forms.DockStyle.Fill;

            //this.panelGameCanvas.Location = new System.Drawing.Point(0, 0);
            //this.panelGameCanvas.Size = new System.Drawing.Size(500, 300);
            this.panelGameCanvas.Dock = System.Windows.Forms.DockStyle.Fill;

            //this.panelSettings.Location = new System.Drawing.Point(0, 0);
            //this.panelSettings.Size = new System.Drawing.Size(500, 300);
            this.panelSettings.Dock = System.Windows.Forms.DockStyle.Fill;

            panelSettings.Hide();
            panelGameCanvas.Hide();
            panelMainMenu.Show();
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Alt)
            {
                FullScreen();
                e.SuppressKeyPress = true;
            }
            else if (e.KeyCode == Keys.Escape)
            {
                Pause();
            }
        }

        private void Pause()
        {
            panelGameCanvas.Hide();
            panelMainMenu.Show();
        }

        private void FullScreen()
        {
            if (FormBorderStyle == FormBorderStyle.Sizable)
            {
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
            }
            else
            {
                FormBorderStyle = FormBorderStyle.Sizable;
                WindowState = FormWindowState.Normal;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            panelMainMenu.Hide();
            panelSettings.Show();
            //MessageBox.Show("Game Start!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StreamReader reader = null;

            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                DialogResult dlgResult = dialog.ShowDialog();
                if (dlgResult != DialogResult.OK)
                    return;

                reader = new StreamReader(dialog.FileName);
                string pointsFileContent = reader.ReadLine();

                _points = Bezier.Points(pointsFileContent);

                textBox1.Clear();

                for (int i = 0; i < _points.Count; i++)
                {
                    textBox1.AppendText(string.Format("{0}, {1}\r\n", _points[i].X, _points[i].Y));
                }

                DrawBezier();
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        private void DrawBezier()
        {
           PointFDraw[] points = new PointFDraw[_points.Count];

            int dX = int.Parse(textBoxDX.Text);
            int dY = int.Parse(textBoxDY.Text);

            for (int i = 0; i < points.Length; i++)
            {
                points[i].X = _points[i].X + dX;
                points[i].Y = _points[i].Y + dY;
            }

            Graphics g = panelSettings.CreateGraphics();
            Pen blackPen = new Pen(Color.Black, 1);

            g.Clear(SystemColors.Control);

            for (int i = 0; i + 3 < points.Length; i += 3)
            {
                g.DrawBezier(blackPen, points[i + 0], points[i + 1], points[i + 2], points[i + 3]);
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            DrawBezier();
        }

        private void buttonDrawByPoints_Click(object sender, EventArgs e)
        {
            PointFDraw[] points = new PointFDraw[_points.Count];

            int dX = int.Parse(textBoxDX.Text);
            int dY = int.Parse(textBoxDY.Text);

            for (int i = 0; i < points.Length; i++)
            {
                points[i].X = _points[i].X + dX;
                points[i].Y = _points[i].Y + dY;
            }

            Graphics g = panelSettings.CreateGraphics();
            Pen blackPen = new Pen(Color.Black, 1);

            g.Clear(SystemColors.Control);

            float step = float.Parse(textBoxBezierStep.Text);

            for (int i = 0; i + 3 < points.Length; i += 3)
            {
                //g.DrawBezier(blackPen, points[i + 0], points[i + 1], points[i + 2], points[i + 3]);
                DrawBezier(g, points[i + 0], points[i + 1], points[i + 2], points[i + 3], step);
            }
        }

        private void DrawBezier(Graphics g, PointFDraw P0, PointFDraw P1, PointFDraw P2, PointFDraw P3, float step)
        {
            PointFDraw p;
            PointFDraw tmp = P0;

            SolidBrush brush = new SolidBrush(Color.Red);
            Pen pen = new Pen(Color.Red);

            for(float t = 0.0F; t <= 1.0F; t+=step)
            {
                float k0 = (float)Math.Pow(1 - t, 3);
                float k1 = (float)(3 * Math.Pow(1 - t, 2) * t);
                float k2 = (float)(3 * (1 - t) * Math.Pow(t, 2));
                float k3 = (float)Math.Pow(t, 3);

                p = P0.Multiply(k0)
                    .Add(P1.Multiply(k1))
                    .Add(P2.Multiply(k2))
                    .Add(P3.Multiply(k3));

                g.FillRectangle(brush, p.X, p.Y, 1, 1);
                //g.DrawLine(pen, p, tmp);

                tmp = p;
            }
        }
    }
}
