﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zuma.GameEngine
{
    public interface IGameDrawing
    {
        void DrawBoard(Field board);
        void DrawPath(IPath path);
        void DrawBall(Ball ball);
        void DrawFrog(Frog frog);
        void DrawBonus(Bonus bonus);
    }
}
