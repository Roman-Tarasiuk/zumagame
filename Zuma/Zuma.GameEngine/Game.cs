﻿using System;

namespace Zuma.GameEngine
{
    public class Game
    {
        private Field _field;
        private IPath _path;


        public Game(string iniFile)
        {
            LoadIniFile(iniFile);
        }

        private void LoadIniFile(string iniFile)
        {
            
        }

        public void Play()
        {

        }
    }

    public enum GameStatus
    {
        Initialized,
        Playing,
        Paused,
        Over
    }
}
