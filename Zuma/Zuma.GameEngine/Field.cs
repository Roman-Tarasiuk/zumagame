﻿using System;

namespace Zuma.GameEngine
{
    public class Field
    {
        public Size Size { get; private set; }

        public Field( Size size)
        {
            this.Size = size;
        }

        public Field(int width, int height)
        {
            this.Size = new Size(width, height);
        }
    }
}
