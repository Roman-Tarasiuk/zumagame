﻿using System;

namespace Zuma.GameEngine
{
    public class Ball
    {
        public PointF Centre { get; set; }
        public float Radius { get; set; }

        public BallType Type { get; set; }
        public BallBonus Bonus { get; set; }
        public BallColor Color { get; set; }

        public float Distance(Ball ball)
        {
            return (float)Math.Sqrt(Math.Pow(Centre.X - ball.Centre.X, 2) + Math.Pow(Centre.Y - ball.Centre.Y, 2));
        }

        public float Distance(PointF point)
        {
            return (float)Math.Sqrt(Math.Pow(Centre.X - point.X, 2) + Math.Pow(Centre.Y - point.Y, 2));
        }

        public bool Intersects(Ball ball)
        {
            return this.Distance(ball) <= Radius + ball.Radius;
        }
    }

    public enum BallType
    {
        Normal,
        Shooting,
        Bonus,
        OutOfField
    }

    public enum BallBonus
    {
        None,
        Bomb,
        MoveSlow,
        MoveBack
    }

    public enum BallColor
    {
        Red,
        Green,
        Blue,
        Yellow,
        Plum,
        Silver
    }
}
